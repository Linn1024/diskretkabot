import requests
import io
import csv
from typing import Callable, Tuple, List

import db
from common import ScoresCallback, GroupPattern, CourseAlias, CourseId, StudentName
from logs import log

def byname(url: str, groups: GroupPattern, course: CourseAlias, field: str = "Ответы") -> ScoresCallback:
    def download() -> List[Tuple[CourseId, StudentName, int]]:
        courseid = db.get_courses(course)[0].id
        data = requests.get(url).content.decode('utf8')
        doc = csv.DictReader(io.StringIO(data))
        res = []
        for d in doc:
            try:
                res.append((courseid, d['ФИО'], int(float(d[field]))))
            except Exception:
                log.exception("During /refresh of {} {}: {}".format(groups, course, d))
        return res
    return download

def ads3(url: str, groups: GroupPattern, course: CourseAlias) -> ScoresCallback:
    def download() -> List[Tuple[CourseId, StudentName, int]]:
        courseid = db.get_courses(course)[0].id
        data = requests.get(url).content.decode('utf8')
        doc = list(csv.reader(io.StringIO(data)))
        head, doc = doc[0], doc[1:]
        name_idx = head.index('ФИО')
        sum_idx = head.index('∑')
        def norm(s: str) -> str:
            if '(' in s:
                return s[:s.index('(')].strip()
            return s
        res = []
        for d in doc:
            if not d[name_idx]:
                continue
            try:
                res.append((courseid, StudentName(norm(d[name_idx])), int(float(d[sum_idx]))))
            except Exception:
                log.exception("During /refresh of {} {}: {}".format(groups, course, d))
        return res
    return download
    
