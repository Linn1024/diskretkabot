PORT = 8443
NAME = "diskretkabot"
HOST = "akomarov.org"
KEYFILE = "./{}.key".format(NAME)
CERTFILE= "./{}.pem".format(NAME)
DB = "test.db"
TOKEN = open("token").read().strip()
API = "https://api.telegram.org/bot{}/".format(TOKEN)
API_FILE = "https://api.telegram.org/file/bot{}/".format(TOKEN)

from common import GroupPattern, CourseAlias

from gdocdownloader import byname, ads3
GDOC_CSV = [
    byname(
        "https://docs.google.com/spreadsheets/d/1pRyn03kKWs9FXu_zOSQ0f81T17FNkBp7XomoGnyequg/export?format=csv&id=1pRyn03kKWs9FXu_zOSQ0f81T17FNkBp7XomoGnyequg&gid=0",
        GroupPattern("M313."),
        CourseAlias("ДМ")
    ),
    byname(
        "https://docs.google.com/spreadsheets/d/14MNqS3pfpEqAZbR9L0UBykVftq7r0QFJXJA_2ernq8E/export?format=csv&id=14MNqS3pfpEqAZbR9L0UBykVftq7r0QFJXJA_2ernq8E&gid=0",
        GroupPattern("M323."),
        CourseAlias("ДМ")
    ),
    byname(
        "https://docs.google.com/spreadsheets/d/1MZdh6VRJ9BZSxkCV5iG9AoNe99Ct1ZhFu9Wjbfmg4fk/export?format=csv&id=1MZdh6VRJ9BZSxkCV5iG9AoNe99Ct1ZhFu9Wjbfmg4fk&gid=1675925088",
        GroupPattern("M313[23]"),
        CourseAlias("АСД"),
        "Ответы у доски"
    ),
    ads3(
        "https://docs.google.com/spreadsheets/d/1MZdh6VRJ9BZSxkCV5iG9AoNe99Ct1ZhFu9Wjbfmg4fk/export?format=csv&id=1MZdh6VRJ9BZSxkCV5iG9AoNe99Ct1ZhFu9Wjbfmg4fk&gid=1916280332",
        GroupPattern("M313[4-7]"),
        CourseAlias("АСД")
    ),
    byname(
        "https://docs.google.com/spreadsheets/d/1MZdh6VRJ9BZSxkCV5iG9AoNe99Ct1ZhFu9Wjbfmg4fk/export?format=csv&id=1MZdh6VRJ9BZSxkCV5iG9AoNe99Ct1ZhFu9Wjbfmg4fk&gid=0",
        GroupPattern("M313[89]"),
        CourseAlias("АСД"),
        "Семинары"
    ),
    ads3(
        "https://docs.google.com/spreadsheets/d/1nWBk2wJOKr1mz_8MhmnSQHIrI0DUXLKj_qZO7IayzTo/export?format=csv&id=1nWBk2wJOKr1mz_8MhmnSQHIrI0DUXLKj_qZO7IayzTo&gid=1916280332",
        GroupPattern("M323[4-7]"),
        CourseAlias("АСД")
    )
#    byname("https://docs.google.com/spreadsheets/d/1nWBk2wJOKr1mz_8MhmnSQHIrI0DUXLKj_qZO7IayzTo/export?format=csv&id=1nWBk2wJOKr1mz_8MhmnSQHIrI0DUXLKj_qZO7IayzTo&gid=0", "M323[89]", "АСД", "Семинары")
]
