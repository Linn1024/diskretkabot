import config

import logging
import logging.handlers
import sys
if sys.platform != "win32":
    handler = logging.handlers.SysLogHandler(address = "/dev/log")
log = logging.getLogger(config.NAME)
log.setLevel(logging.DEBUG)
if sys.platform != "win32":
    log.addHandler(handler)

