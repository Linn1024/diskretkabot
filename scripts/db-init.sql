DROP TABLE IF EXISTS requests;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS tgid_to_studentid;
DROP TABLE IF EXISTS scores;
DROP TABLE IF EXISTS solved_problems;
DROP TABLE IF EXISTS is_admin;
DROP TABLE IF EXISTS state;
DROP TABLE IF EXISTS feedback;
DROP TABLE IF EXISTS schedules;
DROP TABLE IF EXISTS courses;
DROP TABLE IF EXISTS selected_courses;
DROP TABLE IF EXISTS group_aliases;

CREATE TABLE requests (
       data TEXT,
       timestamp TEXT DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE groups (
       name TEXT
);

CREATE TABLE students (
       name TEXT,
       groupid INTEGER,
       FOREIGN KEY(groupid) REFERENCES groups(rowid)
);

CREATE TABLE tgid_to_studentid (
       tgid INTEGER PRIMARY KEY ON CONFLICT REPLACE,
       studentid INTEGER,
       FOREIGN KEY(studentid) REFERENCES students(rowid)
);

CREATE TABLE scores (
       studentid INTEGER,
       courseid INTEGER,
       score INTEGER,
       PRIMARY KEY(studentid, courseid) ON CONFLICT REPLACE,
       FOREIGN KEY(studentid) REFERENCES students(rowid),
       FOREIGN KEY(courseid) REFERENCES courses(rowid)
);

CREATE TABLE solved_problems2 (
       studentid INTEGER,
       courseid INTEGER,
       problems TEXT,
       timestamp TEXT DEFAULT CURRENT_TIMESTAMP,
       FOREIGN KEY(studentid) REFERENCES students(rowid),
       FOREIGN KEY(courseid) REFERENCES courses(rowid)
);

CREATE TABLE is_admin (
       tgid INTEGER PRIMARY KEY
);

CREATE TABLE state (
       tgid INTEGER PRIMARY KEY ON CONFLICT REPLACE,
       state TEXT
);

CREATE TABLE feedback (
       tgid INTEGER,
       text TEXT,
       timestamp TEXT DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE schedules (
       tgid INTEGER,
       problem INTEGER,
       studentid INTEGER,
       status TEXT,
       PRIMARY KEY (tgid, problem, studentid),
       FOREIGN KEY(studentid) REFERENCES students(rowid)
);

CREATE TABLE courses (
       name TEXT,
       alias TEXT
);

CREATE TABLE selected_courses (
       tgid INTEGER PRIMARY KEY ON CONFLICT REPLACE,
       courseid INTEGER,
       FOREIGN KEY(courseid) REFERENCES courses(rowid)
);

CREATE TABLE group_aliases (
       alias TEXT
);

INSERT INTO groups(name) VALUES ("M3134"), ("M3135"), ("M3136"), ("M3137"), ("M3138"), ("M3139");

INSERT INTO groups(name) VALUES ("M3236"), ("M3237"), ("M3238"), ("M3239");

INSERT INTO group_aliases(alias) VALUES
       ("M323[67]");

INSERT INTO students(name, groupid) VALUES
       ("Абрамов Артем Андреевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Амиров Ильдар Ринатович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Безбородов Ян Алексеевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Беликов Дмитрий Романович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Бондарчук Юрий Павлович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Брильянтов Вадим Александрович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Глезденев Александр Викторович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Даминов Нодир Зокирович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Довжик Лев Игоревич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Ерохина Виктория Сергеевна", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Козлов Кирилл Олегович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Коротеев Григорий Михайлович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Листвин Федор Дмитриевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Мелеховец Максим Николаевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Михайлов Никита Дмитриевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Наумов Станислав Сергеевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Петровский Александр Валерьевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Путилин Михаил Андреевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Чекашев Антон Анатольевич", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Яковлев Илья Владимирович", (SELECT rowid FROM groups WHERE name = "M3139")),
       ("Баев Иван Александрович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Бин Дарья Игоревна", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Бойцов Егор Валерьевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Ваксман Денис Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Ванькович Иван Вячеславович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Виноградов Виктор Константинович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Демченко Данил Андреевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Ельманов Григорий Константинович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Звягинцева Дарья Александровна", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Зеленов Максим Николаевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Исрафилов Данил Ирфатович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Кокорин Илья Всеволодович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Начкин Олег Дмитриевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Панкратов Дмитрий Витальевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Пережогин Никита Алексеевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Слоневский Степан Васильевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Староверов Даниил Глебович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Угай Янис Александрович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Финагеев Дмитрий Юрьевич", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Хусаинов Анвер Русланович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Яценко Андрей Владиславович", (SELECT rowid FROM groups WHERE name = "M3138")),
       ("Байдюк Вадим Михайлович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Борисов Алексей Михайлович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Варламов Михаил Алексеевич", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Вахрушев Кирилл Константинович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Вознов Петр Алексеевич", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Воробьев Александр Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Енгулатов Дмитрий Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Жевтяк Нина Вадимовна", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Ионов Дмитрий Павлович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Кравченко Александр Дмитриевич", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Крылов Дмитрий Павлович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Кузьмин Иван Вадимович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Куриленко Владислав Юрьевич", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Кухтеня Алексей Михайлович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Ларионов Никита Романович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Пахомов Дмитрий Сергеевич", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Пилярчук Анна Борисовна", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Трусиенко Дмитрий Вячеславович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Шушарин Дмитрий Романович", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Юнг Егор Васильевич", (SELECT rowid FROM groups WHERE name = "M3137")),
       ("Буланов Кирилл Сергеевич", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Буренков Олег Витальевич", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Волков Александр Михайлович", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Глухов Евгений Дмитриевич", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Гнедаш Александр Сергеевич", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Головин Павел Андреевич", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Карлукова Марина Валерьевна", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Кондратьев Егор Вадимович", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Макеев Петр Александрович", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Наговицын Родион Станиславович", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Нечаев Владислав Антонович", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Осипов Александр Алексеевич", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Попыркина Мария Романовна", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Россомахина Арина Игоревна", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Сайфулин Владислав Олегович", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Санникова Александра Дмитриевна", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Санникова Наталья Дмитриевна", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Тельной Кирилл Павлович", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Тимошенко Егор Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Федер Евгений Александрович", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Шостина Анастасия Дмитриевна", (SELECT rowid FROM groups WHERE name = "M3136")),
       ("Амиров Фарит Ринатович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Балашов Ярослав Дмитриевич", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Дроздов Глеб Анатольевич", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Иванов Сергей Сергеевич", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Ильин Игорь Сергеевич", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Копица Антон Владимирович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Малофеев Иван Дмитриевич", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Михайленко Кристина Игоревна", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Нефедов Евгений Александрович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Николаев Александр Геннадьевич", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Пакулев Александр Данилович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Парфенов Максим Александрович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Передреев Дмитрий Александрович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Полозенко Александра Валентиновна", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Рыкунов Николай Викторович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Сахипов Илья Константинович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Устинов Александр Владимирович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Халимов Руслан Рафитович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Хлебников Валентин Николаевич", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Хромова Ксения Игоревна", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Цыганов Павел Олегович", (SELECT rowid FROM groups WHERE name = "M3135")),
       ("Волошин Никита Юрьевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Галкин Егор Георгиевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Гатин Айрат Алмазович", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Гламозда Виталий Олегович", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Дымарчук Пётр Петрович", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Касаткина Анна Сергеевна", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Кириченко Степан Юрьевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Лейни Юлия Сергеевна", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Леонов Дмитрий Сергеевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Лисицына Александра Константиновна", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Максимов Тимофей Алексеевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Минин Александр Александрович", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Пако Крикун Эдвин Евгений", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Певнев Григорий Сергеевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Прудников Степан Александрович", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Руденко Александр Максимович", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Тельнов Сергей Андреевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Терещук Максим Андреевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Хужин Павел Андреевич", (SELECT rowid FROM groups WHERE name = "M3134")),
       ("Челпанов Дмитрий Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3134"));

INSERT INTO students(name, groupid) VALUES
       ("Беринчик Вадим Вячеславович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Будин Николай Алексеевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Бураков Иван Александрович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Елькин Максим Сергеевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Игнашов Иван Максимович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Кириллов Дмитрий Сергеевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Киселев Владислав Александрович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Ковальчук Михаил Андреевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Колчанов Александр Андреевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Корчагин Максим Анатольевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Костливцев Никита Алексеевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Лиференко Даниил Андреевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Мельников Роман Вячеславович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Родионова Анна Дмитриевна", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Романова Апполинария Алексеевна", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Слепкова Наталия Дмитриевна", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Смородин Роман Александрович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Соколова Мария Викторовна", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Торопин Константин Игоревич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Тураев Мехрубон Ниёзмахмадович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Шалагин Артем Алексеевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Шахбазян Гарик Айкович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Шкарупин Данил Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Ягламунов Владислав Радикович", (SELECT rowid FROM groups WHERE name = "M3239")),
       ("Антонов Кирилл Александрович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Архипов Денис Юрьевич", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Валидов Нияз Алмазович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Воробьев Никита Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Воробьева Анна Юрьевна", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Глотов Михаил Александрович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Иванов Артем Борисович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Карбушев Кирилл Павлович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Климович Иван Сергеевич", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Кузнецов Егор Андреевич", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Портнов Владимир Владимирович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Радченко Светлана Андреевна", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Рябчиков Глеб Вадимович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Самарин Антон Сергеевич", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Сомов Артем Владимирович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Султанов Азат Фаритович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Супрун Екатерина Антоновна", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Тихонов Юрий Михайлович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Трофимов Иван Вячеславович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Тукалло Александр Кириллович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Тупикина Дарья Сергеевна", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Ширвинский Александр Борисович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Ширинкин Арсений Владимирович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Юрлов Михаил Антонович", (SELECT rowid FROM groups WHERE name = "M3238")),
       ("Аверин Максим Игоревич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Акулов Михаил Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Анисимов Алексей Васильевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Анисимов Алексей Витальевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Воробьев Александр Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Гилев Павел Андреевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Голчин Роман Алексеевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Григорьев Михаил Тимофеевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Жевнеров Максим Вячеславович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Земцов Владислав Витальевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Зенович Артем Леонидович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Кареньков Игорь Дмитриевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Кириченко Андрей Вячеславович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Макаренко Егор Алексеевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Мельников Анатолий Сергеевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Михайлов Никита Юрьевич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Нурдинов Ростислав Артурович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Плотников Андрей Игоревич", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Сафронов Егор Германович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Серафимова Ксения Анатольевна", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Стоянов Дмитрий Александрович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Титова Софья Сергеевна", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Хаятов Олег Илхомович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Шакиев Александр Эдгарович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Шаханович Дмитрий Олегович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Юсупов Рузель Альфредович", (SELECT rowid FROM groups WHERE name = "M3237")),
       ("Гаврилов Сергей Сергеевич", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Гера Станислава Игоревна", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Горбатов Егор Александрович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Гузев Иван Вадимович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Егоров Антон Николаевич", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Исламов Айдар Альбертович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Караваев Виталий Евгеньевич", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Колесова Александра Евгеньевна", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Лазичный Иван Александрович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Левина Полина Вячеславовна", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Ляпин Максим Петрович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Мальцев Антон Александрович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Осипов Станислав Михайлович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Писаренко Александр Александрович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Попыркин Даниил Сергеевич", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Савельев Игорь Вадимович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Сафиулин Нариман Шухраталиевич", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Сокол Алексей Андреевич", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Страшков Андрей Сергеевич", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Татанов Октай Кириллович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Тимошенко Егор Вячеславович", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Топаев Тимур ", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Фисков Роман Алексеевич", (SELECT rowid FROM groups WHERE name = "M3236")),
       ("Шандуренко Константин Владимирович", (SELECT rowid FROM groups WHERE name = "M3236"));

INSERT INTO is_admin VALUES (3392586), (67019611);

INSERT INTO courses(name, alias) VALUES
       ("Дискретная математика", "ДМ"),
       ("Алгоритмы и структуры данных", "АСД");

INSERT INTO scores
       SELECT students.rowid, courses.rowid,0
       FROM students JOIN courses;
