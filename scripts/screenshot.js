var page = require('webpage').create(),
    system = require('system');

if (system.args.length != 3) {
    console.log('Usage: screenshot.js <input> <output>');
    phantom.exit(1);
}

input = system.args[1];
output = system.args[2];

page.open(input, function() {
  page.render(output);
  phantom.exit();
});
