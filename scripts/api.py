import requests
import json

from common import *
from logs import *

def send_message(tgid: TgId, text: str, keyboard: List[List[str]] = None) -> None:
    log.debug("Sending to {}: {}".format(tgid, text))
    json = {
        'chat_id': tgid,
        'parse_mode': 'markdown',
        'text': text
    }
    if keyboard:
        json['reply_markup'] = {
            'keyboard': [[{'text': text} for text in row] for row in keyboard],
            'one_time_keyboard': True
        }
    log.debug("json: {}".format(json))
    r = requests.post(config.API + "sendMessage", json=json)
    log.debug("Response: {}".format(r.text))

def send_photo(tgid: TgId, photo_path: str) -> None:
    log.debug("Sending to {} photo {}".format(tgid, photo_path))
    requests.post(config.API + "sendPhoto?chat_id={}".format(tgid),
                  files={"photo": open(photo_path, "rb")}
    )

def send_video(tgid: TgId, video_path: str) -> None:
    log.debug("sending to {} video {}".format(tgid, video_path))
    requests.post(config.API + "sendVideo?chat_id={}".format(tgid),
                  files={"video": open(video_path, "rb")}
    )

def get_file(file_id: str) -> bytes:
    log.debug("get_file('{}')".format(file_id))
    r = requests.post(config.API + 'getFile', json = {
        'file_id': file_id})
    result = json.loads(r.text)
    r = requests.get(config.API_FILE + result['result']['file_path'])
    log.debug("{}".format(r))
    return r.content

def describe(req: UpdateRequest) -> str:
    data = req.json.get("message", {}).get("from", {})
    first_name = data.get("first_name", "")
    last_name = data.get("last_name", "")
    if first_name and last_name:
        res = first_name + " " + last_name
    else:
        res = first_name + last_name
    username = data.get("username", "")
    if res and username:
        res = "{} aka @{}".format(res, username)
    elif username:
        res = "@" + username
    if not res:
        tgid = data.get("id", "")
        res = "id {}".format(tgid)
    return res
        
